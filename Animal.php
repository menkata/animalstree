<?php

abstract class Animal {
	private $animalName;
	private $animalType;
	private $animalWeight;
	private $foodEaten;


	/**
		*Animal constructor
		*@param string animalName
		*@param string animalType
		*@param double animalWeight
		*@param double foodEaten
	*/
	public function __construct(string $animalName,string $animalType, float $animalWeight,float $foodEaten)
	{
		$this->setAnimalName($name);
		$this->setAnimalType($type);
		$this->setAnimalWeight($weight);
		$this->setFoodEaten($food);
	}

	//setters
	public function setAnimalName(string $name)
	{
		$this->animalName = $name;
	}

	public function setAnimalType(string $type)
	{
		$this->animalType = $type;
	}

	public function setAnimalWeight(float $weight)
	{
		$this->animalWeight = $weight;
	}

	public function setFoodEaten(float $food)
	{
		$this->foodEaten = $food;
	}

	//gettesrs
	public function getAnimalName()
	{
		return $this->animalName;
	}

	public function getAnimalType()
	{
	
		return $this->animalType;
	}

	public function eat()
	{
		if($this->animalType == 'Mommal')
			{
				echo 'you eat vegetables';
			}
		elseif($this->animalType == 'Felime')
			{
				echo "You eat Meat";
			}
	}

	public function getAnimalWeight()
	{
		return $this->animalWeight;
	}

	public function getFoodEaten()
	{
		return $this->foodEaten;
	}

	//abstract methods
	public function makeSOund()
	{

	}
	public function eatFood($food)
	{

	}

}

?>